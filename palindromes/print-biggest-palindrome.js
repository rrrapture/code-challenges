//loop through numbers from 2 - lowest prime - to 1000

function isPrime(value) {
	for(var j = 2; j < value; j++) {
		if(value % j === 0) {
			return false;
		}
	}
	return value > 1;
}

function isBiggestPalindrome(limit) {

	for(var i = limit; i > 2; i--) {
		var stringified, length;
		
		stringified = i.toString();
		stringifiedLength = stringified.length;

		//palindrome test if number is 3 digits long
		if(stringifiedLength === 3) {
			if(stringified[0] === stringified[stringifiedLength - 1]) {
				console.log('palindrome: ' + stringified);
				//console.log(isPrime(stringified));
				// isPrime(stringified);
				if( isPrime(stringified) ){
					//console.log('this is our prime palindrome: ' + stringified);
					break;
				}
			}
		}
	}
	return stringified;	
}




//************
//brute force

for(var i = 1000; i > 0; i--){
	var stringified = i.toString();
	var length = stringified.length;
	//palindrome test
	//only works for length of 3
	if( length === 3 ){
		if(stringified[0] === stringified[length - 1]) {
			console.log('stringified found first and last value of index are a match: ' + stringified);

			//primality - brute force tests
			var divisionBy2 = stringified % 2;
			var divisionBy3 = stringified % 3;
			var divisionBy5 = stringified % 5;
			var divisionBy7 = stringified % 7;
			var divisionBy11 = stringified % 11;
			var divisionBy13 = stringified % 13;
			var divisionBy17 = stringified % 17;
			var divisionBy19 = stringified % 19;
			var divisionBy23 = stringified % 23;
			var divisionBy29 = stringified % 29;
			var divisionBy31 = stringified % 31;
			var divisionBy37 = stringified % 37;

			console.log('stringified modulus 2 is ' + divisionBy2);
			console.log('stringified modulus 3 is ' + divisionBy3);
			console.log('stringified modulus 5 is ' + divisionBy5);
			console.log('stringified modulus 7 is ' + divisionBy7);

			console.log('stringified modulus 11 is ' + divisionBy11);
			console.log('stringified modulus 13 is ' + divisionBy13);
			console.log('stringified modulus 17 is ' + divisionBy17);

			console.log('stringified modulus 19 is ' + divisionBy19);
			console.log('stringified modulus 23 is ' + divisionBy23);

			console.log('stringified modulus 29 is ' + divisionBy29);
			console.log('stringified modulus 31 is ' + divisionBy31);
			console.log('stringified modulus 37 is ' + divisionBy37);

			// should not be cleanly divisible by 2,3,5 or 7, etc.
			// if any of these moduli is equal to zero, we do NOT have a prime number, so go on to the next palindrome
			// if none of these moduli are zero, we have a match! exit loop
			// hmm my logic is flawed - I'm leaving out other numbers like 11, 13, etc. where to stop?
			if( !(stringified % 2 === 0) 
				&& !(stringified % 3 === 0) 
				&& !(stringified % 5 === 0) 
				&& !(stringified % 7 === 0) 
				&& !(stringified % 11 === 0) 
				&& !(stringified % 13 === 0) 
				&& !(stringified % 17 === 0) 
				&& !(stringified % 23 === 0)  
				&& !(stringified % 29 === 0) 
				&& !(stringified % 31 === 0)  
				&& !(stringified % 37 === 0) ) {
				console.log('stringified of ' + stringified + ' cannot be divided evenly by the prime numbers above');
				break;
			}
		}
	}
}







// *************

for(var i = 1000; i > 0; i--){
	var stringified = i.toString();
	var length = stringified.length;

	//palindrome test
	if(stringified[0] === stringified[length - 1]) {
	  console.log('stringified found first and last value of index are a match: ' + stringified);

		//should not be cleanly divisible by 2,3,5 or 7
		//crude prime number test
		if( !(stringified % 2 === 0) && !(stringified % 3 === 0) && !(stringified % 5 === 0) && !(stringified % 7 === 0) ) {
			console.log('stringified of ' + stringified + ' cannot be divided evenly by 2 or 3 or 5 or 7');
		}

		break;
	}
}







//http://michalbe.blogspot.com/2013/03/javascript-less-known-parts-bitwise.html


for(var i = 1000; i > 0; i--){
	var stringified = i.toString();
	var length = stringified.length;
	if(stringified[0] === stringified[length - 1]) {
	  alert('stringified found 0 and last a match ' + stringified + ' ' + );
	  break;
	}
}



********





for(var i = 1000; i > 0; i--){
	var stringified = i.toString();
	var length = stringified.length;
	if(stringified[0] === stringified[length - 1]) {
	  alert('stringified found 0 and last a match ' + stringified);
	  break;
	}
}

< stops looping through all the damn numbers



for(var i = 1000; i > 0; i--){
	var stringified = i.toString();
	var length = stringified.length;
	if(stringified[0] === stringified[length - 1]) {
	  alert('stringified found 0 and last a match ' + stringified);
	}
}


var limit = 1000;
929

biggestPalindrome
aba

where first character is same as last character

second character is equal to second to last character

third character is equal to 3rd to last character

if length is 3

we look at numbers as a string (? makes sense?) convert number to string

make numbers an array?
or can i just evaluate index positions and length of the string?

then we evaluate index 0, index 1, index 2

then if index 0 character is equal to index 2 character

we don't have to look at index 1


