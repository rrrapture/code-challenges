requirements:
1. Write a program that prints the numbers from 1 to 100. 
2. But for multiples of three print “Fizz” instead of the number 
3. and for the multiples of five print “Buzz”. 
4. For numbers which are multiples of both three and five print “FizzBuzz”."





trying with switch statement:


didn't get this right at first - have to pass true
see: Switch matches the x in switch(x){ to the result of evaluating the case expressions. since all your cases will result in true /false there is no match and hence default is executed always.

now using switch for your problem is not recommended because in case of too many expressions there may be multiple true outputs thus giving us unexpected results. But if you are hell bent on it :



********
function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    switch (true) {
      case (i % 3 === 0 && i % 5 === 0):
        console.log("FizzBuzz");
        break;
      case (i % 3 === 0):
        console.log("Fizz");
        break;
      case (i % 5 === 0):
        console.log("Buzz");
        break;
      default:
        console.log(i);
    }
  }
}

fizzBuzzChallenge();


********

only prints default 1-100:
function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    switch (i) {
      case (i % 3 === 0 && i % 5 === 0):
        console.log("FizzBuzz");
        break;
      case (i % 3 === 0):
        console.log("Fizz");
        break;
      case (i % 5 === 0):
        console.log("Buzz");
        break;
      default:
        console.log(i);
    }
  }
}

fizzBuzzChallenge();

******

(*********************)



*****

good one with if:

function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
      console.log("FizzBuzz");
    } else if( i % 3 === 0 ) {
      console.log("Fizz");
    } else if( i % 5 === 0 ) {
      console.log("Buzz");
    } else {
      console.log(i);
    }
  }
}

fizzBuzzChallenge();




note - i guess 0 modulus 3 and 5 is 0, but I don't think zero is a multiple of 3 or 5...
********

returns:
FizzBuzz
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
Fizz
Buzz
26
Fizz
28
29
FizzBuzz
31
32
Fizz
34
Buzz
Fizz
37
38
Fizz
Buzz
41
Fizz
43
44
FizzBuzz
46
47
Fizz
49
Buzz
Fizz
52
53
Fizz
Buzz
56
Fizz
58
59
FizzBuzz
61
62
Fizz
64
Buzz
Fizz
67
68
Fizz
Buzz
71
Fizz
73
74
FizzBuzz
76
77
Fizz
79
Buzz
Fizz
82
83
Fizz
Buzz
86
Fizz
88
89
FizzBuzz
91
92
Fizz
94
Buzz
Fizz
97
98
Fizz
Buzz

that's it. 
********

oops I missed #1 that first time. note to self always write out requirements.

function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
      console.log("FizzBuzz");
    } else if( i % 3 === 0 ) {
      console.log("Fizz");
    } else if( i % 5 === 0 ) {
      console.log("Buzz");
    }
  }
}

fizzBuzzChallenge();


function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
      console.log("FizzBuzz i is " + i);
    } else if( i % 3 === 0 ) {
      console.log("Fizz i is " + i);
    } else if( i % 5 === 0 ) {
      console.log("Buzz i is " + i);
    }
  }
}

fizzBuzzChallenge();

<< cleaner

function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
      console.log("FizzBuzz i is " + i);
    } else if( i % 3 === 0 ) {
      console.log("Fizz i is " + i);
    } else if( i % 5 === 0 ) {
      console.log("Buzz i is " + i);
    } else {
      console.log("Something is broken, i is " + i);
    }
  }
}

fizzBuzzChallenge();

<< verbose but works









first try lol IFFY borked!
(fizzBuz = function fizzBuzz {
    for(var i; i <= 100; i++) {
        console.log(i);
    }
})();
<broke!





function fizzBuzz () {
  var i;
  for (i = 0; i <= 100; i++) {
    if(i % 3 === 0 && i % 5 === 0)
    if(i % 3 === 0) {
      console.log("Fizz! i is " + i);
    } elseif (i % 5 === 0) {
      console.log("Buzz! i is " + i);
    } 
  }
}
fizzBuzz();

<hmm what broke?

function fizzBuzzChallenge () {
  var i;
  for (i = 0; i <= 100; i++) {
    if( i % 3 === 0 && i % 5 === 0 ) {
      console.log("FizzBuzz i is " + i);
    } else if( i % 3 === 0 ) {
      console.log("Fizz i is " + i);
    } else if( i % 5 === 0 ) {
      console.log("Buzz i is " + i);
    } else {
      console.log("Something is broken, i is " + i);
    }
  }
}
